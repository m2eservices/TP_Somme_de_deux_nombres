package com.example.jeanclaude.tpsommededeuxnombres

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // les nombres
        val txt_nb1 = findViewById<View>(R.id.txt_nb1) as EditText
        val txt_nb2 = findViewById<View>(R.id.txt_nb2) as EditText
        val txt_resultat = findViewById<View>(R.id.lbl_resultat) as TextView

        // le bouton
        val btn_Somme = findViewById<View>(R.id.btn_Somme) as Button
        btn_Somme.setOnClickListener(View.OnClickListener {
            val nb1: Int
            //nb1 = Integer.parseInt(txt_nb1.getText() + "");
            //nb1 = Integer.parseInt(txt_nb1.getText().toString());

            // pour gérer les pbs associés aux chaines vides par exemple... (qui fait crasher l'appli !)
            try {
                nb1 = Integer.parseInt(txt_nb1.text.toString())
            } catch (e: NumberFormatException) {
                Toast.makeText(applicationContext, "saisissez le premier nombre svp", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }

            val nb2: Int
            nb2 = Integer.parseInt(txt_nb2.text.toString() + "")

            /*try {
					nb2 = Integer.parseInt(txt_nb2.getText()+"");
				} catch (NumberFormatException e) {
					Toast.makeText(getApplicationContext(), "saisissez le second nombre svp", Toast.LENGTH_SHORT).show();
					return;
				}*/

            val total = nb1 + nb2
            txt_resultat.text = total.toString()
        })
    }
}